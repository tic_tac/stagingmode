# Install app

## build app

Executer le fichier `build.sh` et vous verrez une image docker dans le registry de votre projet google.

> attention penser à mondifier le project ID de votre fichier build.sh

```docker
docker build -t eu.gcr.io/<project ID>/hello-world .
docker push eu.gcr.io/<project ID>/hello-world
```

![Registry google](./google_registry.png)

## official app

Pour installer l'application, executer :

```helm
helm install helm/hello-world-backend/ --name hello-world
```

## another backend

Pour installer l'application, executer :

```helm
helm install helm/another-hello-world-backend/ --name another-hello-world
```

## Staging backend

Pour installer l'application, executer :

```helm
helm install helm/staging-hello-world-backend/ --name staging-hello-world
```

## Bonus

```sh
.
├── helm
│   ├── another-hello-world-backend
│   │   ├── templates
│   │   │   ├── deployment.yaml
│   │   │   └── services.yaml
│   │   ├── Chart.yaml
│   │   └── values.yaml
│   ├── hello-world-backend
│   │   ├── templates
│   │   │   ├── deployment.yaml
│   │   │   └── services.yaml
│   │   ├── Chart.yaml
│   │   └── values.yaml
│   └── staging-hello-world-backend
│       ├── templates
│       │   ├── deployment.yaml
│       │   └── services.yaml
│       ├── Chart.yaml
│       └── values.yaml
├── server.py
├── build.sh
├── Dockerfile
└── README.md
```